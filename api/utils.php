<?php
if (!defined("APP")) {
	header('HTTP/1.0 403 Forbidden');
	return;
}

function setGame($game) {
	$_SESSION['game'] = $game;
}

function &getGame() {
	return $_SESSION['game'];
}

function setPlayer($player) {
	$_SESSION['player'] = $player;
}

function getPlayer() {
	return $_SESSION['player'];
}

function checkSessionPlayer() {
	$check = FALSE;
	if (isset($_SESSION['player'])) {
		$check = TRUE;
	}
	return $check;
}

function checkSessionGame() {
	$check = FALSE;
	if (isset($_SESSION['game'])) {
		$check = TRUE;
	}
	return $check;
}