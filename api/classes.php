<?php
if (!defined("APP")) {
	header('HTTP/1.0 403 Forbidden');
	return;
}
require_once "init.php";

class Player {
	public $id;
	public $name;

	public function __construct($name = NULL) {
		if ($name !== NULL) $this->checkPlayer($name);
	}

	private function checkPlayer($name) {
		$player = $this->selectPlayerByName($name);
		if (!$player) {
			$iId = $this->addPlayer($name);
			$player = $this->selectPlayerById($iId);
		}
		$this->id = intval($player['id']);
		$this->name = $player['name'];
	}

	private function addPlayer($name) {
		DB::run('INSERT INTO players (name) VALUES (?)', array($name));
		return DB::lastInsertId('players');
	}

	private function selectPlayerByName($name) {
		return DB::run('SELECT * FROM players WHERE name = ?', array($name))->fetch();
	}

	private function selectPlayerById($id) {
		return DB::run('SELECT * FROM players WHERE id = ?', array($id))->fetch();
	}

	public function toArray() {
		$playerInfo = array(
				"id"   => intval($this->id),
				"name" => $this->name
		);
		return $playerInfo;
	}
}

class Game {
	public  $id            = 0;
	public  $player        = NULL;
	public  $date          = NULL;
	public  $fields        = array();
	public  $countMoves    = 0;
	public  $duration      = 0;
	private $baseSequence  = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
	private $solveSequence = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0);

	public function __construct($player) {
		$this->player = $player;
	}

	public function newGame() {
		$this->generateGame();
		$this->save(TRUE);
	}

	private function generateGame() {
		$ar = $this->baseSequence;
		shuffle($ar);
		array_push($ar, 0);
		if (!$this->testSolvable($ar))
			$ar = $this->swap(0, 1, $ar);
		$this->fields = $ar;
	}

	private function testSolvable($ar) {
		for ($kDisorder = 0, $i = 1, $len = count($ar) - 1; $i < $len; $i++)
			for ($j = $i - 1; $j >= 0; $j--) if ($ar[$j] > $ar[$i]) $kDisorder++;
		return !($kDisorder % 2);
	}

	private function swap($el1, $el2, $arr) {
		$t = $arr[$el1];
		$arr[$el1] = $arr[$el2];
		$arr[$el2] = $t;
		return $arr;
	}

	public function save($new = FALSE) {
		if ($new) {
			DB::run('INSERT INTO games (player_id, fields) VALUES (?, ?)',
							array($this->player->id, json_encode($this->fields)));
			$lastId = DB::lastInsertId('games');
			$this->loadGame($lastId);
		} else {
			DB::run('UPDATE games SET fields = ?, count_moves = ?, duration = ? WHERE id = ?', array(
																																													 json_encode($this->fields),
																																													 $this->countMoves,
																																													 $this->duration,
																																													 $this->id
																																											 )
			);
		}
	}

	public function loadGame($gameId) {
		$game = $this->selectGame($gameId);
		$this->id = intval($game['id']);
		$this->date = $game['date'];
		$this->fields = json_decode($game['fields']);
		$this->countMoves = intval($game['count_moves']);
		$this->duration = intval($game['duration']);
	}

	private function selectGame($gameId) {
		$game = DB::run('SELECT * FROM games WHERE id = ?', array($gameId))->fetch();
		return $game;
	}

	public static function deleteGame($gameId) {
		return DB::run('DELETE FROM games WHERE id = ?', array($gameId));
	}

	public function setMove($x, $y, $duration) {
		$this->duration = $duration;
		$canMove = FALSE;
		$isWin = FALSE;
		$acceptedMove = array(-4, -1, 1, 4);
		$indexMove = ($x * 4) + $y;
		$nullPosition = $this->getNullPosition();
		if (in_array($indexMove - $nullPosition, $acceptedMove)) {
			$canMove = TRUE;
			$this->countMoves++;
			$this->fields = $this->swap($indexMove, $nullPosition, $this->fields);
			$isWin = $this->fields == $this->solveSequence;
		}
		$resp = array("can_move" => $canMove, "is_win" => $isWin);
		if ($isWin) {
			$result = new Result();
			$result->saveResult(array($this->player->id, $this->countMoves, $duration));
			$lastInsertId = $lastId = DB::lastInsertId('results');
			$res = Result::selectResult($lastInsertId);
			$resp['result'] = $res;
		}
		return $resp;
	}

	private function getNullPosition() {
		return array_search(0, $this->fields);
	}

	public static function getAll($playerId) {
		return DB::run('SELECT g.id, p.name as player, date, count_moves, duration 
													FROM games g
													INNER JOIN players p ON g.player_id = p.id
													WHERE g.player_id = ?', array($playerId))->fetchAll();
	}

	public static function finishedGame($gameId) {
		return DB::run('UPDATE games SET is_finished = 1 WHERE id = ?', array($gameId));
	}

	public static function deleteUnmovedGames($currentGameId) {
		return DB::run('DELETE FROM games WHERE count_moves = 0 AND duration = 0 AND id != ?', array($currentGameId));
	}

	public function toArray() {
		$gameInfo = array(
				"id"         => $this->id,
				"player"     => $this->player->toArray(),
				"date"       => $this->date,
				"fields"     => $this->fields,
				"countMoves" => $this->countMoves,
				"duration"   => $this->duration
		);
		return $gameInfo;
	}
}

class Result {
	public function __construct() {

	}

	public function saveResult($savedResult) {
		DB::run('INSERT INTO results (player_id, count_moves, duration) VALUES (?, ?, ?)', $savedResult);
	}

	public static function getAll() {
		return DB::run('SELECT r.id, p.name as player, date, count_moves, duration FROM results r
													INNER JOIN players p ON r.player_id = p.id')->fetch();
	}

	public static function selectResult($id) {
		return DB::run('SELECT id, date, duration, count_moves FROM results WHERE id = ?', array($id))->fetch();
	}

	public static function clearResults() {
		return DB::run('TRUNCATE TABLE results');
	}
}