<?php
define('APP', "SAPPER");
require_once "init.php";
require_once "utils.php";
require_once "classes.php";

session_start();

$postData = file_get_contents('php://input');
$data = json_decode($postData, TRUE);
header('Content-Type: application/json');

$action = $data['action'];

$response = array(
		"status" => "error"
);

switch ($action) {
	case 'init':
		if (checkSessionPlayer()){
			$player = getPlayer();
			if (checkSessionGame()) {
				$game = &getGame();
				Game::deleteUnmovedGames($game->id);
			} else {
				$game = null;
				Game::deleteUnmovedGames(0);
			}
			$response['status'] = "success";
			$response['data'] = array(
					"player" => $player->toArray(),
					"game" => $game !== null ? $game->toArray() : null,
			);
		} else {
			$response['status'] = "error";
		}
		break;
	case 'getPlayer':
		if ($data['name'] != "") {
			$player = new Player($data['name']);
			$response['status'] = 'success';
			$response['data'] = $player->toArray();
			setPlayer($player);
		} else {
			$response['status'] = "error";
			$response['message'] = "Empty name";
		}
		break;
	case 'startNewGame':
		$player = getPlayer();
		$game = new Game($player);
		$game->newGame();
		setGame($game);
		$response['status'] = 'success';
		$response['data'] = $game->toArray();
		break;
	case 'saveGame':
		$duration = intval($data['duration']);
		$game = &getGame();
		$game->duration = $duration;
		$game->save();
		$response['status'] = 'success';
		$response['data'] = $game->toArray();
		break;
	case 'getGames':
		$player = getPlayer();
		$games = Game::getAll($player->id);
		$response['status'] = 'success';
		$response['data'] = $games;
		break;
	case 'getGame':
		$gameId = intval($data['game_id']);
		$player = getPlayer();
		$game = new Game($player);
		$game->loadGame($gameId);
		setGame($game);
		$response['status'] = 'success';
		$response['data'] = $game->toArray();
		break;
	case 'deleteGame':
		$gameId = intval($data['game_id']);
		if (Game::deleteGame($gameId)) {
			$response['status'] = 'success';
			$response['message'] = "Game " . $gameId . " successfully deleted";
		} else {
			$response['status'] = 'error';
			$response['message'] = "Game " . $gameId . " is not found";
		}
		break;
	case 'testFinishGame':
		$game = &getGame();
		$game->fields = [1,2,3,4,5,6,7,8,9,10,12,15,13,14,11,0];
		$response['status'] = 'success';
		$response['data'] = $game->toArray();
		break;
	case 'setMove':
		$x = intval($data['x']);
		$y = intval($data['y']);
		$duration = intval($data['duration']);
		$game = &getGame();
		$move = $game->setMove($x, $y, $duration);
		if ($move['can_move']) {
			$response['status'] = 'success';
			$response['is_win'] = $move['is_win'];
			if ($move['is_win']){
				$response['data'] = $move['result'];
			}
			$response['message'] = "Move successfully done";
		} else {
			$response['status'] = 'error';
			$response['message'] = "Move failed";
		}
		break;
	case 'getResults':
		$results = Result::getAll();
		if ($results === false) $results = array();
		$response['status'] = 'success';
		$response['data'] = $results;
		break;
	case 'clearResults':
		if (Result::clearResults()) {
			$response['status'] = 'success';
			$response['message'] = "Results is cleared";
		} else {
			$response['status'] = 'error';
			$response['message'] = "Error in clear results";
		}
		break;
	default:
		$response['status'] = "error";
		$response['message'] = "Undefined action request";
		break;
}

echo json_encode($response);