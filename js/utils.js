$.fn.serializeObject = function () {
	let o = {};
	let a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function sendRequest(action, data, callbackSuccess) {
	let baseActionUrl = '/api/handler.php';
	data.action = action;
	$.ajax({
					 url        : baseActionUrl,
					 data       : JSON.stringify(data),
					 dataType   : "json",
					 method     : "POST",
					 contentType: "application/json; charset=utf-8",
					 success    : callbackSuccess
				 });
}

function openModal(id, isClose = true) {
	$('#' + id).modal({
											fadeDuration: 100,
											escapeClose : isClose,
											clickClose  : isClose,
											showClose   : isClose
										});
}

function renderFields(fields) {
	console.log("RENDER FIELDS: " + fields);
	$('#pyatna_field td').removeClass('empty');
	fields.forEach((el, index) => {
		if (el !== 0) {
			$('#pyatna_field td').eq(index).text(el);
		} else {
			$('#pyatna_field td').eq(index).addClass('empty').text('');
		}
	});
}

function renderSavedGames(games) {
	let saveGamesBlock = $('#savedGamesList');
	saveGamesBlock.html('');
	if (games.length > 0) {
		games.forEach((el) => {
			let game = $('<div/>').addClass('gameItem').attr('id', 'game_' + el.id).attr('game_id', el.id);
			game.html(
					`<span>${el.id}</span><span>${el.player}</span><span>${el.date}</span><span>${el.duration}</span><span>${el.count_moves}</span>`);
			saveGamesBlock.append(game);
		});
	}
}

function renderTime(duration = 0) {
	let rblock = $('#duration');
	let ost = duration;
	let h = parseInt(ost / 3600);
	ost = ost - (h * 3600);
	let m = parseInt(ost / 60);
	ost = ost - (m * 60);
	let s = ost;
	let hh = h.toString().length === 1 ? '0' + h.toString() : h.toString();
	let mm = m.toString().length === 1 ? '0' + m.toString() : m.toString();
	let ss = s.toString().length === 1 ? '0' + s.toString() : s.toString();
	rblock.text(hh + ":" + mm + ":" + ss);
}

function renderCountMoves(countMoves = 0) {
	rblock = $('#count_moves').text(countMoves);
}

function renderResults(results) {
	let resultsBlock = $('#recordsList');
	resultsBlock.html('');
	if (results.length > 0) {
		results.forEach((el) => {
			let record = $('<div/>').addClass('recordItem').attr('id', 'record_' + el.id);
			record.html(
					`<span>${el.id}</span><span>${el.player}</span><span>${el.date}</span><span>${el.duration}</span><span>${el.count_moves}</span>`);
			resultsBlock.append(record);
		});
	}
}

function swapField(coordsField, coordsEmpty) {
	let field = $('#pyatna_field td[ri="' + coordsField.ri + '"][ci="' + coordsField.ci + '"]');
	let emptyField = $('#pyatna_field td[ri="' + coordsEmpty.ri + '"][ci="' + coordsEmpty.ci + '"]');
	emptyField.removeClass('empty');
	emptyField.text(field.text());
	field.text('');
	field.addClass('empty');
}

function move(game, ri, ci, emptyCoords) {
	game.setMove(ri, ci, (st, resultData) => {
		if (st === 1) {
			swapField({ri: ri, ci: ci}, emptyCoords);
		} else if (st === 2) {
			swapField({ri: ri, ci: ci}, emptyCoords);
			$('#res_id').text(resultData.id);
			$('#res_date').text(resultData.date);
			$('#res_duration').text(resultData.duration);
			$('#res_count_moves').text(resultData.count_moves);
			game.isLoaded = false;
			game.stopTimer();
			openModal('win_game');
		}
	});
}