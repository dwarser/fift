let game = {};

$(document).ready(function () {
	$('#setName').on('submit', function (e) {
		let $this = $(this);
		e.preventDefault();
		let name = $('input[name="name"]', $this).val();
		game.getPlayer(name, () => {
			this.reset();
			$.modal.close();
		});
	});

	$('#newGame').on('click', function (e) {
		e.preventDefault();
		game.startNewGame();
	});

	$('#saveGame').on('click', function (e) {
		e.preventDefault();
		game.saveGame();
	});

	$('#savedGamesList').on('click', '.gameItem', function (e) {
		e.preventDefault();
		let gameId = $(this).attr('game_id');
		game.loadGame(gameId);
	});

	$('#deleteGame').on('click', function (e) {
		e.preventDefault();
		let gameId = $(this).attr('gameid');
		game.deleteGame(gameId);
	});

	$('#savedGames').on('click', function (e) {
		e.preventDefault();
		openModal('save_game');
	});

	$('#savedGamesList').on('click', '.gameItem', function (e) {
		e.preventDefault();

	});

	$('#clearResults').on('click', function (e) {
		e.preventDefault();
		game.clearResults();
	});

	$('#game_field').on('click', '#pyatna_field td:not(.empty)', function (e) {
		let ri = parseInt($(this).attr('ri'));
		let ci = parseInt($(this).attr('ci'));
		let emptyCoords = game.getEmptyField();
		e.preventDefault();
		move(game, ri, ci, emptyCoords);
	});

	$(document).on('keydown', function (e) {
		console.log(e.keyCode);
		if (e.keyCode === 82) {
			e.preventDefault();
			openModal('records');
		} else if (e.keyCode === 84) {
			sendRequest('testFinishGame', {}, (response) => {
				game.fields = response.data.fields;
				renderFields(game.fields);
			});
		} else if (e.keyCode === 38){ //UP
			let emptyCoords = game.getEmptyField();
			let ri = emptyCoords.ri + 1;
			let ci = emptyCoords.ci;
			if (ri > 3){
				console.log('Can\'t move');
			} else {
				move(game, ri, ci, emptyCoords);
			}
		} else if (e.keyCode === 40){ //DOWN
			let emptyCoords = game.getEmptyField();
			let ri = emptyCoords.ri - 1;
			let ci = emptyCoords.ci;
			if (ri < 0){
				console.log('Can\'t move');
			} else {
				move(game, ri, ci, emptyCoords);
			}
		} else if (e.keyCode === 37){ //LEFT
			let emptyCoords = game.getEmptyField();
			let ri = emptyCoords.ri;
			let ci = emptyCoords.ci + 1;
			if (ci > 3){
				console.log('Can\'t move');
			} else {
				move(game, ri, ci, emptyCoords);
			}
		} else if (e.keyCode === 39){ //RIGHT
			let emptyCoords = game.getEmptyField();
			let ri = emptyCoords.ri;
			let ci = emptyCoords.ci - 1;
			if (ci < 0){
				console.log('Can\'t move');
			} else {
				move(game, ri, ci, emptyCoords);
			}
		}
	});

	init();
});

function init() {
	game = new Game();
}