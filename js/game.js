class Game {

	constructor() {
		this.id = 0;
		this.player = null;
		this.date = null;
		this.fields = [];
		this.countMoves = 0;
		this.duration = 0;
		this.savedGamesList = [];
		this.resultsList = [];
		this.counter = null;
		this.isLoaded = false;
		this.isStarted = false;
		this.checkInitFromSession();
	}

	checkInitFromSession() {
		sendRequest('init', {}, (response) => {
			if (response.status === "success") {
				console.log('Player is initiated');
				let game = response.data.game !== null ? response.data.game : false;
				let player = response.data.player;
				this.player = player;
				if (game) {
					console.log('Game is initiated');
					this.id = game.id;
					this.date = game.date;
					this.fields = game.fields;
					this.countMoves = game.countMoves;
					this.duration = game.duration;
					this.isLoaded = true;
					renderFields(this.fields);
					renderCountMoves(this.countMoves);
					renderTime(this.duration);
					console.log('Game is loaded');
				}
				this.initSavedGames();
				this.initResults();
			} else {
				console.log("Session is not initiated");
				this.initPlayer();
			}
		});
	}

	initPlayer() {
		openModal('set_name', false);
	}

	initSavedGames() {
		sendRequest('getGames', {}, (response) => {
			if (response.status === "success") {
				this.savedGamesList = response.data;
				renderSavedGames(this.savedGamesList);
			} else {
				console.log('Error in load games');
			}
		});
	}

	initResults() {
		sendRequest('getResults', {}, (response) => {
			if (response.status === "success") {
				this.resultsList = response.data ? response.data : [];
				renderResults(this.resultsList);
			}
		});
	}

	clearResults() {
		sendRequest('getResults', {}, (response) => {
			if (response.status === "success") {
				this.resultsList = [];
			}
		});
	}

	getPlayer(name, finished) {
		sendRequest('getPlayer', {name: name}, (response) => {
			if (response.status === "success") {
				this.player = response.data;
			} else {
				console.log(response.message);
			}
			finished();
		});
	}

	startNewGame() {
		sendRequest('startNewGame', {}, (response) => {
			if (response.status === "success") {
				let game = response.data;
				this.id = parseInt(game.id);
				this.date = game.date;
				this.fields = game.fields;
				this.countMoves = 0;
				this.duration = 0;
				this.isLoaded = true;
				this.isStarted = false;
				this.stopTimer();
				renderFields(this.fields);
				renderCountMoves(this.countMoves);
				renderTime(this.duration);
			}
		});
	}

	saveGame() {
		if (!this.isLoaded) return false;
		sendRequest('saveGame', {duration: this.duration}, (response) => {
			if (response.status === "success") {
				let game = response.data;
				let fitem = this.savedGamesList.find((el) => el.id === game.id);
				if (fitem !== undefined) {
					fitem.duration = game.duration;
					fitem.countMoves = game.countMoves;
				} else {
					this.savedGamesList.push(game);
				}
			}
		});
	}

	loadGame(gameID) {
		this.isLoaded = false;
		sendRequest('getGame', {game_id: gameID}, (response) => {
			if (response.status === "success") {
				let game = response.data;
				this.id = game.id;
				this.date = game.date;
				this.fields = game.fields;
				this.duration = game.duration;
				this.countMoves = game.countMoves;
				this.isLoaded = true;
				this.isStarted = false;
				this.stopTimer();
				renderCountMoves(this.countMoves);
				renderTime(this.duration);
				renderFields(this.fields);
				$.modal.close();
			}
		});
	}

	deleteGame(gameID) {
		sendRequest('deleteGame', {game_id: gameID}, (response) => {
			if (response.status === "success") {
				console.log(response.message);
			} else {
				console.log(response.message);
			}
		});
	}

	setMove(ri, ci, cb) {
		if (!this.isLoaded) return false;
		if (ri < 0 || ri > 3 || ci < 0 || ci > 3) return false;
		sendRequest('setMove', {x: ri, y: ci, duration: this.duration}, (response) => {
			let st = 0;
			let resultData = null;
			if (response.status === "success") {
				st = 1;
				let indexField = ri * 4 + ci;
				let indexEmpty = this.fields.indexOf(0);
				[this.fields[indexField], this.fields[indexEmpty]] = [this.fields[indexEmpty], this.fields[indexField]];
				if (!this.isStarted) {
					this.isStarted = true;
					this.startTimer();
				}
				game.countMoves += 1;
				renderCountMoves(this.countMoves);
			} else {
				console.log('Can\'t move this element');
			}
			if (response.is_win) {
				st = 2;
				resultData = response.data;
			}
			cb(st, resultData);
		});
	}

	getEmptyField() {
		let emptyIndex = this.fields.indexOf(0);
		let coords = {};
		coords.ri = parseInt(emptyIndex / 4);
		coords.ci = parseInt(emptyIndex % 4);
		return coords;
	}

	startTimer() {
		this.counter = setInterval(() => {
			this.duration++;
			renderTime(this.duration);
		}, 1000);
	}

	stopTimer() {
		if (this.counter !== null) {
			clearInterval(this.counter);
			this.counter = null;
		}
	}
}